#![forbid(unsafe_code)]
extern crate rand;
use std::{
    fs::File,
    io::{BufRead, BufReader, Read},
    thread,
    time
};

#[derive(Debug)]
pub struct Tarpit {
    black_list: Vec<String>,
    ips: Vec<String>,
}
 
impl Tarpit {
    pub fn new(f: &str, ips: Vec<String>) -> Self {
        let black_list = match File::open(f) {
            Ok(file) => {
                let buf_reader = BufReader::new(file);
                buf_reader.lines()
                    .map(|l| { l.unwrap() } )
                    .collect()
            },
            Err(_e) => { panic!("couldn't load suspicious urls") }
        };
        Tarpit { black_list, ips}
    }

    pub fn known(&self, uri: &str) -> Option<&String> {
        self.black_list.iter()
            .find(| u | u.as_str() == uri )      
    }
}

pub struct TarpitReader { delay: time::Duration }
impl TarpitReader {
    pub fn new(delay: u64) -> Self {
        TarpitReader{ delay: time::Duration::from_millis(delay) }
    }
}
impl Read for TarpitReader {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        loop {
            buf.iter()
                .map( |_|rand::random::<u8>());
            thread::sleep(self.delay);
        }
        Ok(0)
    }
}


#[cfg(test)]
mod tests {
    use crate::Tarpit;
    #[test]
    fn it_works() {
        let tp = Tarpit::default();
        assert_eq!(tp.known("/hmseo.php"), Some(&"/hmseo.php".to_string()) );
    }
}
