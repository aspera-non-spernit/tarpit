# tarpit

bog down scans for vulnerabilities.

If you are running a web server, (ie. rocket, actix) and your service is constantly scanned for
php, asp vulnerabilities, this is for you.

tarpit uses a simple text file, a list of paths that have known vulnerabilities that could be exploited.

if a scanned path matches one of these, tarpit simplifies to return a stream of random values.
it also halts the thread to slow down the client.

### Usage

1. Create a ```Tarpit``` instance
2. in a 404 catch check if requested path is in list of suspicious urls
3. redirect to a "tarpit"-route.
4. Create a ```TarpitReader``` instance
5. Return a Stream


```rust
// Cargo.toml
tarpit = {git="https://gitlab.com/aspera-non-spernit/tarpit", branch="dev"}


// rocket example
extern crate tarpit;
use tarpit::{Tarpit, TarpitReader};

// tarpit route
#[get("/secret/access")]                        // fool with the hacker noobs
fn tarpit() -> Stream<TarpitReader> {
   Stream::chunked( TarpitReader::new(1000), 64) // 1,000 ms delay after each chunk of 64 bytes
}

// catch 404
#[catch(404)]
fn not_found(req: &Request) -> Redirect {
    let tp = Tarpit::new("suspicious_paths");   // file with suspicious urls
    if tp.known(&req.uri().path()).is_some() {
        Redirect::to(uri!(bog_down))            // assuming deliberate scan
    } else {
        Redirect::to(uri!(index))               // assuming regular, accidental 404
    }
}

// dependent on your web framework. here rocket.rs
rocket::ignite()
    .register(catchers![not_found])          // add 404 catcher
    .mount("/", routes![tarpit, ..]          // add tarpit route
    ..
```
